FROM bitnami/git:2.38.1
WORKDIR /git
RUN git clone https://gitlab.com/AKSHAYREPOSITORY/opstreejava.git

FROM maven:3-jdk-8-alpine
COPY --from=0 /git/opstreejava /opstreejava
WORKDIR /opstreejava
RUN mvn clean package -DskipTests=true


FROM tomcat:9.0.8-jre8-alpine
COPY --from=1 /opstreejava/target/Spring3HibernateApp.war /usr/local/tomcat/webapps/ 
RUN sed -i 's/8080/9090/' /usr/local/tomcat/conf/server.xml
EXPOSE 9090/tcp
